### dashchan-indiachan
a dashchan extention for indiachan.io

### note
i am not an android developer. so i just monkey patched till it worked. contributions are welcomed.

### links
- download from releases [page](https://codeberg.org/charsi/dashchan-indiachan/releases)
- forked from https://github.com/im663/Dashchan-Extension-LynxChan-Generic
- used this guide https://im663.com/how-to-add-app-support-for-lynxchan-imageboards-through-dashchan/
